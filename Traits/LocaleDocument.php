<?php

namespace Nitra\StoreBundle\Traits;

use Gedmo\Mapping\Annotation as Gedmo;

trait LocaleDocument
{
    /**
     * @Gedmo\Locale
     */
    protected $locale;

    public function setTranslatableLocale($locale)
    {
        $this->locale = $locale;
    }
}