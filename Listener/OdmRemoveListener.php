<?php

namespace Nitra\StoreBundle\Listener;

use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\Container;

/**
 * Odm remove listener.
 * Запрет на удаление связаных документов.
 */
class OdmRemoveListener
{
    /** @var Container */
    protected $container;

    public function __construct(Container $container)
    {
        $this->container        = $container;
    }

    /**
     * @return \Monolog\Logger
     */
    protected function getLogger() { return $this->container->get('logger'); }

    public function preSoftDelete(LifecycleEventArgs $args)
    {
        $this->preRemove($args);
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $dm                     = $args->getDocumentManager();
        $currentDocument        = $args->getDocument();
        $currentDocumentClass   = get_class($currentDocument);
        $isRefExists            = null;

        $documentClassNames = $dm->getConfiguration()
            ->getMetadataDriverImpl()
            ->getAllClassNames();

        foreach ($documentClassNames as $documentClassName) {
            try {
                $cm = $dm->getClassMetadata($documentClassName);

                foreach ($cm->getAssociationNames() as $associationName) {
                    $atc = $cm->getAssociationTargetClass($associationName);
                    if (($currentDocumentClass == $atc) || is_subclass_of($currentDocumentClass, $atc)) {
                        $searchObj = $dm->getRepository($documentClassName)->findOneBy(array(
                            $associationName . '.$id'   => new \MongoId($currentDocument->getId()),
                        ));
                        if ($searchObj) {
                            $isRefExists = sprintf("Found link from \"%s\" (\"%s\") in object with id - \"%s\"", $documentClassName, $associationName, $searchObj->getId());
                            break;
                        }
                    }
                }
            } catch(\Exception $e) {
                $this->getLogger()->error($e->getMessage());
            }
        }

        if ($isRefExists) {
            $message = 'Reference_error: ' . $isRefExists;
            $this->getLogger()->error($message);

            throw new \Exception($message);
        }
    }
}