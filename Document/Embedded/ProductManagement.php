<?php

namespace Nitra\StoreBundle\Document\Embedded;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Настрйки копирования товаров
 * @ODM\EmbeddedDocument
 */
class ProductManagement
{
    /**
     * @var string Идентификатор
     * @ODM\Id
     */
    protected $id;

    /**
     * @var array Игноририуемые при коприовании поля
     * @ODM\Hash
     */
    protected $ignoreCopyFields = array();

    /**
     * @var array Общие поля для товаров модели
     * @ODM\Hash
     */
    protected $commonProductsFields = array();

    /**
     * @var array Формирование полного имени товара
     * @ODM\Hash
     */
    protected $fullProductName = array();

    /**
     * @var array Формирование сокарщенного имени товара
     * @ODM\Hash
     */
    protected $shortProductName = array();

    /**
     * @var array Формирование полного имени модели
     * @ODM\Hash
     */
    protected $fullModelName = array();

    /**
     * @var array Формирование сокарщенного имени модели
     * @ODM\Hash
     */
    protected $shortModelName = array();

    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ignoreCopyFields
     * @param array $ignoreCopyFields
     * @return self
     */
    public function setIgnoreCopyFields(array $ignoreCopyFields)
    {
        $this->ignoreCopyFields = $ignoreCopyFields;
        return $this;
    }

    /**
     * Get ignoreCopyFields
     * @return array $ignoreCopyFields
     */
    public function getIgnoreCopyFields()
    {
        return $this->ignoreCopyFields;
    }

    /**
     * Set commonProductsFields
     * @param array $commonProductsFields
     * @return self
     */
    public function setCommonProductsFields(array $commonProductsFields)
    {
        $this->commonProductsFields = array_values($commonProductsFields);
        return $this;
    }

    /**
     * Get commonProductsFields
     * @return array
     */
    public function getCommonProductsFields()
    {
        return $this->commonProductsFields;
    }

    /**
     * Set fullProductName
     * @param array $fullProductName
     * @return self
     */
    public function setFullProductName(array $fullProductName)
    {
        $this->fullProductName = array_values($fullProductName);
        return $this;
    }

    /**
     * Get fullProductName
     * @return array
     */
    public function getFullProductName()
    {
        $fullProductName = $this->fullProductName;
        $multisort = array();
        foreach ($fullProductName as $key => $value) {
            $multisort[$key] = (int) $value['order'];
        }
        array_multisort($multisort, SORT_ASC, $fullProductName);

        return $fullProductName;
    }

    /**
     * Set shortProductName
     * @param array $shortProductName
     * @return self
     */
    public function setShortProductName(array $shortProductName)
    {
        $this->shortProductName = array_values($shortProductName);
        return $this;
    }

    /**
     * Get shortProductName
     * @return array
     */
    public function getShortProductName()
    {
        $shortProductName = $this->shortProductName;
        $multisort = array();
        foreach ($shortProductName as $key => $value) {
            $multisort[$key] = (int) $value['order'];
        }
        array_multisort($multisort, SORT_ASC, $shortProductName);

        return $shortProductName;
    }

    /**
     * Set fullModelName
     * @param array $fullModelName
     * @return self
     */
    public function setFullModelName(array $fullModelName)
    {
        $this->fullModelName = array_values($fullModelName);
        return $this;
    }

    /**
     * Get fullModelName
     * @return array
     */
    public function getFullModelName()
    {
        $fullModelName = $this->fullModelName;
        $multisort = array();
        foreach ($fullModelName as $key => $value) {
            $multisort[$key] = (int) $value['order'];
        }
        array_multisort($multisort, SORT_ASC, $fullModelName);

        return $fullModelName;
    }

    /**
     * Set shortModelName
     * @param array $shortModelName
     * @return self
     */
    public function setShortModelName(array $shortModelName)
    {
        $this->shortModelName = array_values($shortModelName);
        return $this;
    }

    /**
     * Get shortModelName
     * @return array
     */
    public function getShortModelName()
    {
        $shortModelName = $this->shortModelName;
        $multisort = array();
        foreach ($shortModelName as $key => $value) {
            $multisort[$key] = (int) $value['order'];
        }
        array_multisort($multisort, SORT_ASC, $shortModelName);

        return $shortModelName;
    }
}