<?php

namespace Nitra\StoreBundle\Document\Embedded;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ODM\EmbeddedDocument
 */
class Skype
{
    /**
     * @var string Идентификатор
     * @ODM\Id
     */
    protected $id;

    /**
     * @var string Логин skype
     * @ODM\String
     * @Assert\NotBlank
     * @Assert\Length(max = 100)
     */
    protected $skype;

    /**
     * @var boolean Видимый
     * @ODM\Boolean
     */
    protected $status;

    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set skype
     * @param string $skype
     * @return self
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;
        return $this;
    }

    /**
     * Get Skype
     * @return string $skype
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * Set status
     * @param boolean $status
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     * @return boolean $status
     */
    public function getStatus()
    {
        return $this->status;
    }
}