<?php

namespace Nitra\StoreBundle\Document\Embedded;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ODM\EmbeddedDocument
 */
class Phone
{
    /**
     * @var string Идентификатор
     * @ODM\Id
     */
    protected $id;

    /**
     * @var string Номер
     * @ODM\String
     * @Assert\NotBlank
     * @Assert\Length(max = 20)
     */
    protected $phoneNumber;

    /**
     * @var string Оператор
     * @ODM\String
     * @Assert\Length(max = 10)
     */
    protected $operator;

    /**
     * @var boolean Видимый
     * @ODM\Boolean
     */
    protected $status;

    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set phoneNumber
     * @param string $phoneNumber
     * @return self
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }

    /**
     * Get phoneNumber
     * @return string $phoneNumber
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set operator
     * @param string $operator
     * @return self
     */
    public function setOperator($operator)
    {
        $this->operator = $operator;
        return $this;
    }

    /**
     * Get operator
     * @return string $operator
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * Set status
     * @param boolean $status
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     * @return boolean $status
     */
    public function getStatus()
    {
        return $this->status;
    }
}