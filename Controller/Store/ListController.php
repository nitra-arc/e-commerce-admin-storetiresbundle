<?php

namespace Nitra\StoreBundle\Controller\Store;

use Admingenerated\NitraStoreBundle\BaseStoreController\ListController as BaseListController;

class ListController extends BaseListController
{
    /**
     * Build query builder
     * @return \Doctrine\MongoDB\Query\Builder
     */
    protected function buildQuery()
    {
        // отображаем только доступные для менеджера магазины
        $stores   = $this->getUser()->getStores();
        $storeIds = array();
        foreach ($stores as $store) {
            $storeIds[] = $store->getId();
        }

        $qb = parent::buildQuery();

        $qb->field('id')->in($storeIds);

        return $qb;
    }
}