<?php

namespace Nitra\StoreBundle\Controller\Store;

use Admingenerated\NitraStoreBundle\BaseStoreController\EditController as BaseEditController;

class EditController extends BaseEditController
{
    /**
     * Create form by type, object and options
     * @param \Symfony\Component\Form\FormTypeInterface $type
     * @param \Nitra\StoreBundle\Document\Store $data
     * @param array $options
     * @return \Symfony\Component\Form\FormInterface
     */
    public function createForm($type, $data = null, array $options = array())
    {
        return parent::createForm($type, $data, $options + array(
            'limits'    => $this->formatLimits($data),
        ));
    }

    /**
     * Format configured limits
     * @param \Nitra\StoreBundle\Document\Store $store
     * @return array
     */
    protected function formatLimits($store)
    {
        $limits = array();

        if ($this->container->hasParameter('product_limits')) {
            foreach ($this->container->getParameter('product_limits') as $lName => $limit) {
                $limits[$lName] = $limit + array(
                    'current'   => key_exists($lName, $store->getLimits())
                        ? $store->getLimits()[$lName]
                        : 0,
                );
            }
        }

        return $limits;
    }
}