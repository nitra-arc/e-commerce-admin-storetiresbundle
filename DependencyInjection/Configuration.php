<?php

namespace Nitra\StoreBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $builder = new TreeBuilder();
        $rootNode = $builder->root('nitra_store')->addDefaultsIfNotSet();

        $this->addPayments($rootNode);

        return $builder;
    }

    /**
     * Add payments configuration to root node
     * @param ArrayNodeDefinition $builder
     */
    protected function addPayments(ArrayNodeDefinition $builder)
    {
        $builder
            ->children()
                ->arrayNode('payments')
                    ->prototype('array')
                        ->children()
                            ->arrayNode('fields')
                                ->cannotBeEmpty()
                                ->isRequired()
                                ->requiresAtLeastOneElement()
                                ->prototype('array')
                                    ->children()
                                        ->scalarNode('label')->defaultNull()->end()
                                        ->scalarNode('help')->defaultNull()->end()
                                        ->scalarNode('required')->defaultTrue()->end()
                                        ->scalarNode('type')->defaultValue('text')->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
    }
}