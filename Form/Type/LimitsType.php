<?php

namespace Nitra\StoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LimitsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        foreach ($options['limits'] as $lName => $limit) {
            $builder->add($lName, 'integer', array(
                'data'  => $limit['current'],
                'label' => 'limits.' . $lName . '.label',
                'help'  => 'limits.' . $lName . '.help',
                'attr'  => array(
                    'min'   => $limit['min'],
                    'max'   => $limit['max'],
                ),
            ));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'limits';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
             'limits' => array()
        ));
    }
}