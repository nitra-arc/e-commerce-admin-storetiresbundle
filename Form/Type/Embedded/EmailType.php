<?php

namespace Nitra\StoreBundle\Form\Type\Embedded;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EmailType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', 'text', array(
            'required' => true,
            'label'    => 'email.email',
        ));

        $builder->add('status', 'checkbox', array(
            'required' => false,
            'label'    => 'email.status',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'embed_email';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'Nitra\\StoreBundle\\Document\\Embedded\\Email',
            'translation_domain' => 'NitraStoreBundle',
        ));
    }
}