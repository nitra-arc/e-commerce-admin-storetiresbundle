<?php

namespace Nitra\StoreBundle\Form\Type\Embedded;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SkypeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('skype', 'text', array(
            'required' => true,
            'label'    => 'skype.skype',
        ));

        $builder->add('status', 'checkbox', array(
            'required' => false,
            'label'    => 'skype.status',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'embed_skype';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'Nitra\\StoreBundle\\Document\\Embedded\\Skype',
            'translation_domain' => 'NitraStoreBundle',
        ));
    }
}