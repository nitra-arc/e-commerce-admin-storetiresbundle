<?php

namespace Nitra\StoreBundle\Form\Type\Embedded;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PhoneType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('phoneNumber', 'text', array(
            'required' => true,
            'label'    => 'phone.number',
        ));
        $builder->add('operator', 'choice', array(
            'choices'  => array(
                'kyivstar'  => 'Kyivstar',
                'mts'       => 'MTS',
                'life'      => 'Life',
                'cdma'      => 'cdma',
                'peoplenet' => 'PEOPLEnet',
                'it'        => 'Интер телеком',
                'utel'      => 'Utel',
                'city'      => 'Городской',
            ),
            'required' => false,
            'label'    => 'phone.operator',
        ));
        $builder->add('status', 'checkbox', array(
            'required' => false,
            'label'    => 'phone.status',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'embed_phone';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => 'Nitra\\StoreBundle\\Document\\Embedded\\Phone',
            'translation_domain' => 'NitraStoreBundle',
        ));
    }
}