<?php

namespace Nitra\StoreBundle\Form\Type\Embedded;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductManagementProductNamesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('order', 'hidden');

        $fnName = 'functionLoadAllowedValues__parentName__' . $builder->getName();
        $builder->add('field', 'genemu_jqueryselect2_hidden', array(
            'label'   => ' ',
            'configs' => array(
                'width' => '400px',
                'query' => $fnName,
            ),
        ));

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) {
            if ($event->getForm()->getParent()) {
                $fnName = 'functionLoadAllowedValues' . $event->getForm()->getParent()->getName() . $event->getForm()->getName();
                $event->getForm()->add('field', 'genemu_jqueryselect2_hidden', array(
                    'label'   => ' ',
                    'configs' => array(
                        'width' => '400px',
                        'query' => $fnName,
                    ),
                ));
            }
        });
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['products']       = $options['products'];
        $view->vars['parameters']     = $options['parameters'];
        $view->vars['category']       = $options['category'];
        $view->vars['parametersType'] = $options['parametersType'];
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'embed_product_management_product_names';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'NitraStoreBundle',
            'products'           => true,
            'parameters'         => false,
            'parametersType'     => 'all',
            'category'           => false,
        ));
    }
}