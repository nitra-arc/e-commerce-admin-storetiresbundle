<?php

namespace Nitra\StoreBundle\Form\Type\Embedded;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PaymentsType extends AbstractType
{
    /**
     * @var array
     */
    protected $payments;

    /**
     * @param array $payments
     */
    public function __construct(array $payments)
    {
        $this->payments     = $payments;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        foreach ($this->payments as $name => $payment) {
            $paymentForm = $builder->create($name, 'form', array(
                'label'     => "store.fields.payments.$name.label",
                'help'      => "store.fields.payments.$name.help",
            ));
            foreach ($payment['fields'] as $field => $options) {
                $paymentForm->add($field, $options['type'], array(
                    'label'     => $options['label'],
                    'help'      => $options['help'],
                    'required'  => $options['required'],
                ));
            }
            $builder->add($paymentForm);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'NitraStoreBundle',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'store_payments';
    }
}