$(document).ready(function(){
    $('.main_menu > ul > li').click(function(){
        $(this).addClass('active').siblings('li.active').removeClass('active');  
        // add class if menu has submenu
        if ($(this).find('.menu_level_2 .current').length > 0) {
            $('.main_menu').addClass('has_submenu_level2');
        } 
        if ($(this).find('.menu_level_2').length == 0) {
            $('.main_menu').removeClass('has_submenu_level2');
        } 
    });
    $('.main_menu > ul > li > ul > li').click(function(){
        $(this).addClass('active').siblings('li.active').removeClass('active');  
        // add class if menu has submenu
        if ($(this).children('ul').length > 0) {
            $('.main_menu').addClass('has_submenu_level2');
        } else {
            $('.main_menu').removeClass('has_submenu_level2');
        }
    });
    
    // определяем текущую страницу
    if ($('li.current').length > 0) {
        $('.main_menu li').removeClass('current');
    }
    if ($('.has_submenu').length > 0) {
        $('.has_submenu').removeClass('has_submenu');
    }
    if ($('.has_submenu_level2').length > 0) {
        $('.has_submenu_level2').removeClass('has_submenu_level2');
    }
    
    var pageHref = window.location.pathname;
    $('.main_menu li a').each(function(){ 
        var linkHref = $(this).attr('href');
        if (pageHref.match(linkHref)){
            $(this).closest('li').addClass('current');
            
            if ($(this).closest('.menu_level_1').length > 0 ) {
                $(this).closest('ul').closest('li').addClass('current');
                $('.main_menu').addClass('has_submenu');
            }
            if ($(this).closest('.menu_level_2').length > 0) {
                $(this).closest('ul').closest('li').closest('ul').closest('li').addClass('current');
                $('.main_menu').addClass('has_submenu_level2');
            }
        }
    })        
});