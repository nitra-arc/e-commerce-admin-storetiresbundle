<?php

namespace Nitra\StoreBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Nitra\StoreBundle\DependencyInjection\Compiler\FormPass;

class NitraStoreBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        
        $container->addCompilerPass(new FormPass());
    }
}