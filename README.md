# StoreBundle

# Конфигурация

```yaml
    # app/config/config.yml
    # ...
    nitra_store:
        payments:
            service_name:
                fields:
                    field_1:
                        label:      label_for_field_1
                        help:       help_for_field_1
                    # ...
                    field_n:
                        label:      label_for_field_n
                        help:       help_for_field_n
    # ...
```

## Описание
* **payments** - конфигурация сервисов для оплаты онлайн
    * **service_name** - имя сервиса оплат
        * **fields** - поля для редактирования
            * **field_1** - имя поля
                * **type** - тип поля (text)
                * **label** - имя поля (null)
                * **help** - подсказка к полю (null)
                * **required** - обязательное ли поле поля (false)

## Пример

```yaml
    # app/config/config.yml
    # ...
    nitra_store:
        payments:
            # Оплата через сервис LiqPay
            liq_pay:
                fields:
                    private_key:
                        label:      store.fields.payments.liq_pay.private_key.label
                        help:       store.fields.payments.liq_pay.private_key.help
                    public_key:
                        label:      store.fields.payments.liq_pay.public_key.label
                        help:       store.fields.payments.liq_pay.public_key.help
    # ...
```

## mongodb ODM store filter
Необходим для того, чтобы по всем коллекциям, у которых есть связь на магазины
Добавлялся фильтр по магазину

```yaml
doctrine_mongodb:
    # ...
    document_managers:
        # ...
        default:
            # ...
            filters:
                # ...
                store:
                    class:          Nitra\StoreBundle\ODMFilters\StoreFilter
                    enabled:        true
                # ...
```