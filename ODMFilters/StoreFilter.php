<?php

namespace Nitra\StoreBundle\ODMFilters;

use Doctrine\ODM\MongoDB\Mapping\ClassMetadata;
use Doctrine\ODM\MongoDB\Query\Filter\BsonFilter;

class StoreFilter extends BsonFilter
{
    /**
     * {@inheritdoc}
     */
    public function addFilterCriteria(ClassMetadata $targetDocument)
    {
        if (!key_exists('storeId', $this->parameters ? : array())) {
            return array();
        }
        $field = $this->getField($targetDocument);

        if (!$field) {
            return array();
        }

        return array(
            '$or' => array(
                array(
                    $field . '.$id' => new \MongoId($this->getParameter('storeId')),
                ),
                array(
                    $field => array(
                        '$exists' => false,
                    ),
                ),
            ),
        );
    }

    /**
     * Check has reference to stores and return field name
     * @param ClassMetadata $targetDocument
     * @return string
     */
    protected function getField(ClassMetadata $targetDocument)
    {
        $field = null;
        if ($targetDocument->reflClass->hasProperty('store')) {
            $field = 'store';
        } elseif ($targetDocument->reflClass->hasProperty('stores')) {
            $field = 'stores';
        }

        return $field;
    }
}